#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_2135.mk

COMMON_LUNCH_CHOICES := \
    lineage_2135-user \
    lineage_2135-userdebug \
    lineage_2135-eng
