#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from 2135 device
$(call inherit-product, device/vivo/2135/device.mk)

PRODUCT_DEVICE := 2135
PRODUCT_NAME := lineage_2135
PRODUCT_BRAND := vivo
PRODUCT_MODEL := V2135
PRODUCT_MANUFACTURER := vivo

PRODUCT_GMS_CLIENTID_BASE := android-vivo-rev1

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="2135i-user 11 RP1A.200720.012 compiler0308215210 release-keys"

BUILD_FINGERPRINT := vivo/2135i/2135:11/RP1A.200720.012/compiler0308215210:user/release-keys
